package com.hugoibm.restAppTest.data;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hugoibm.restAppTest.model.Quiz;

public interface IQuizRepository extends JpaRepository <Quiz, Integer> {

	public Optional<Quiz> findByQuizName(String name);
}
