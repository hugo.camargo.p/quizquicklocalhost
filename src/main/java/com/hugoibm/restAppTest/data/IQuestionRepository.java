package com.hugoibm.restAppTest.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hugoibm.restAppTest.model.Question;

public interface IQuestionRepository extends JpaRepository <Question , Integer> {

}
