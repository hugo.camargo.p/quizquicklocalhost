package com.hugoibm.restAppTest.data;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import com.hugoibm.restAppTest.model.Answer;

public interface IAnswerRepository extends JpaRepository<Answer, Integer> {

    public List<Answer> getAnswerByQuestionId(Integer questionId);
}
