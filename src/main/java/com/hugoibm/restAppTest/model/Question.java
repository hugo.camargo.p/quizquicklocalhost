package com.hugoibm.restAppTest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.Transient;

@Entity
public class Question {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer id;
	private String question;
	private String response;
	private String image;
	
	private Integer quizId;
	
	@Transient
	private int value;
	
	public Question() {
	}
	
	public Question(Integer id, String question, String response, String image) {
		this.id = id;
		this.question = question;
		this.response = response;
		this.image = image;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public Integer getQuizId() {
		return quizId;
	}

	public void setQuizId(Integer quizId) {
		this.quizId = quizId;
	}

	@Override
	public String toString() {
		return String.format("Question [ id:%d , question:%s , reponse:%s , image:%s, quizId:%d ] ", id, question, response, image, quizId);
	}
}
