package com.hugoibm.restAppTest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Answer {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String answerText;
	private String answerImage;
	private boolean correct;
	private Integer questionId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAnswerText() {
		return answerText;
	}
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
	public String getAnswerImage() {
		return answerImage;
	}
	public void setAnswerImage(String answerImage) {
		this.answerImage = answerImage;
	}
	public boolean isCorrect() {
		return correct;
	}
	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	
	@Override
	public String toString() {
		return String.format("Answer [ id:%d, answerText:%s, answerImage:%s, correct:%s]", id, answerText, answerImage, correct);
	}
}
