package com.hugoibm.restAppTest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hugoibm.restAppTest.business.AnswerBusiness;
import com.hugoibm.restAppTest.model.Answer;

@RestController
public class AnswerController {
	@Autowired
	AnswerBusiness answerBusiness;

	@GetMapping("/all-answer")
	public List<Answer> getAllAnswer() {
		return answerBusiness.getAllAnswer();
	}

	@GetMapping("/answerByQuestion/{questionId}")
	public List<Answer> getAnswerByQuestion(@PathVariable("questionId") Integer questionId) {
		return answerBusiness.getAnswerByQuestion(questionId);
	}
}
