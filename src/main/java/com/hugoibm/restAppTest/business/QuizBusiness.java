package com.hugoibm.restAppTest.business;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hugoibm.restAppTest.data.IQuizRepository;
import com.hugoibm.restAppTest.model.Quiz;

@Component
public class QuizBusiness{
	
	@Autowired IQuizRepository iQuizRepository;
	
	public List<Quiz> getAllQuiz(){
		return iQuizRepository.findAll();
	}
	
	public Optional<Quiz> getQuizById(Integer id) {
		return iQuizRepository.findById(id);
	}
	
	public Optional<Quiz> getQuizByQuizName(String name) {
		return iQuizRepository.findByQuizName(name);
	}
}
